import functools

def hash(string, size):
    return int(functools.reduce(lambda a, x: str(a) + str(x), string.encode('utf-8'))) % size

class HashMap:
    def __init__(self):
        self.num_buckets = 200
        self.buckets = [None] * self.num_buckets
    
    def put(self, key, value):
        # get hash for key
        key_hash = hash(key, self.num_buckets)

        if self.buckets[key_hash] is None:
            # initialize bucket if it was empty
            self.buckets[key_hash] = list()
        
        # get a bucket using hash as index
        bucket = self.buckets[key_hash]

        # check if there's any value at the key
        for (i, (k, v)) in enumerate(bucket):
            if k == key:
                # replace value
                bucket[i] = (k, value)
                return
        
        # append key if it didn't exist before
        bucket.append((key, value))
    
    def get(self, key):
        key_hash = hash(key, self.num_buckets)
        bucket = self.buckets[key_hash]
        if bucket is None:
            raise KeyError(key)
        for (k, value) in bucket:
            if k == key:
                return value
    
    def remove(self, key):
        key_hash = hash(key, self.num_buckets)
        bucket = self.buckets[key_hash]
        if bucket is None:
            raise KeyError(key)
        for pair in bucket:
            if pair[0] == key:
                bucket.remove(pair)
        if len(bucket) == 0:
            self.buckets[key_hash] = None

    def __repr__(self):
        s = ''
        for bucket in self.buckets:
            if not bucket is None:
                for (k, v) in bucket:
                    s += "{}: {}, ".format(repr(k), repr(v))
        s = s[:-2]
        s += '}'
        s = '{' + s
        return s

def compare_dict():
    import random
    d = dict()
    h = HashMap()
    for i in range(1000):
        key = str(random.randint(1, 1000000))
        value = i
        d[key] = value
        h.put(key, value)
    any_collision = False
    for key in d:
        dk = d[key]
        hk = h.get(key)
        any_collision = dk != hk

        print('d[{}] = {} | h[{}] = {} | {}'.format(key, dk, key, hk, dk == hk))
    print(any_collision)